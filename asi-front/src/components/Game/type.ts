import {User} from "../../services/api/types/user";
import {RealRoomState} from "../../services/api/types/room";

export interface GameComponentProps {
    user: User;
    roomState: Partial<RealRoomState>;
}