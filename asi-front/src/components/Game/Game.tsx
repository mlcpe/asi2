import {Button, Grid} from "@mui/material";
import GameUserLine from "../components/Line";
import React from "react";
import {StartedRoomState} from "../../services/api/types/room";
import {User} from "../../services/api/types/user";
import {GameComponentProps} from "./type";
import {useGameState, useOwnGameId, useTargetGameId} from "../../services/store";
import {sendEndTurnGameMessage} from "../../pages/play/Play";

interface Props extends GameComponentProps {
    user: User
}

export default function Game({user}: Props) {

    const ownGameId = useOwnGameId()
    const targetGameId = useTargetGameId()
    console.info("targetGameId", targetGameId, ownGameId)

    const roomState = useGameState<StartedRoomState>()


    return (
        <Grid container direction="column" sx={{width: "100%", minHeight: "100vh"}}>
            <Grid item xs={3} sx={{height: "100%", width: "100%"}}>
                <div>Turn :{roomState[roomState.game.turn].login}</div>
                <div>Round : {roomState.game.round - (roomState.game.round % 2)}</div>
            </Grid>

            <GameUserLine gameId={targetGameId} canAttack={false}/>

            <Grid item container xs={2} sx={{height: "100%", width: "100%"}}>
                <Grid item container alignItems="center" justifyContent="center" xs={9}
                      sx={{width: "100%", height: "100%"}}>
                    <hr style={{zIndex: 999, borderTop: "transparent", width: "90%"}}/>
                </Grid>
                <Grid item xs={3} container direction="column" justifyContent="center" alignItems="center"
                      sx={{width: "100%", height: "100%"}}>
                    <Button color="secondary" variant="contained" onClick={sendEndTurnGameMessage}>
                        Passer le tour
                    </Button>
                </Grid>
            </Grid>
            <GameUserLine gameId={ownGameId} canAttack={true}/>

        </Grid>
    );
}
