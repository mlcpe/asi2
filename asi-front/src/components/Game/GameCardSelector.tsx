import React from "react";
import {Button} from "@mui/material";
import TableWrapper from "../Table";
import {headerTable} from "../../pages/sell/Header";
import {sendGameMessage} from "../../pages/play/Play";
import {InterfaceCard} from "../Interfaces/InterfaceCard";
import {GameComponentProps} from "./type";
import {RealRoomState} from "../../services/api/types/room";


interface Props extends GameComponentProps {
    cards: InterfaceCard[];
    roomState: Pick<RealRoomState, "user1" | "user2">;
}

export default function GameCardSelector({roomState, cards}: Props) {
    const [selectedCards, setSelectedCards] = React.useState<number[]>([]);
    const sendCardsMessage = () => sendGameMessage("chooseCard", selectedCards);

    const handleSelect = async (e: any) => {
        if (e.target.value !== undefined && e.target.value) {
            if (selectedCards.includes(e.target.value)) {
                const newSelectedCards = selectedCards.filter((card) => card !== e.target.value);
                setSelectedCards(newSelectedCards);
            } else if (selectedCards.length >= 5) {
                alert("Veuillez retirer une carte avant d'en ajouter une autre")
                return
            } else {
                const newSelectedCards = [...selectedCards, e.target.value];
                setSelectedCards(newSelectedCards);
            }
        }
    }


    return <>
        <Button variant="contained" color="info" size="small"
                sx={{width: "100%", borderRadius: 0}} disabled={!roomState.user1 || !roomState.user2}
                onClick={sendCardsMessage}>
            Valider mes cartes
        </Button>
        <TableWrapper selectedCards={selectedCards.length} isForSelect={true} data={cards}
                      header={headerTable} selectRow={handleSelect}/>
    </>
}