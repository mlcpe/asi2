import Checkbox from '@mui/material/Checkbox';
import { TableCell, TableRow } from "@mui/material";

export default function TableLine({ row, header, selected, selectedRow, indexNumber, isForSelect, selectedCards }: { row: any, header: any, selected?: number, selectedRow: any, indexNumber: number, isForSelect: boolean, selectedCards?: number}) {
    return (
        <>
            {!isForSelect ? (
                <TableRow
                    sx={{ '&:last-child td, &:last-child th': { border: 0 }, ":hover": { cursor: "pointer", bgcolor: "lightgray" }, bgcolor: selected === indexNumber ? "lightblue" : "transparent" }}
                    onClick={() => selectedRow(indexNumber)}
                >
                    {isForSelect && (
                        <TableCell>
                            <Checkbox color="secondary" />
                        </TableCell>
                    )}
                    {header.map((column: any) => (
                        <TableCell key={row[column.col]}>
                            {row[column.col]}
                        </TableCell>
                    ))}
                </TableRow>
            ) : (
                <TableRow
                    sx={{ '&:last-child td, &:last-child th': { border: 0 }, ":hover": { cursor: "pointer", bgcolor: "lightgray" }, bgcolor: selected === indexNumber ? "lightblue" : "transparent" }}
                >
                    {isForSelect && (
                        <TableCell>
                            <Checkbox name="selector" value={indexNumber} color="secondary" onChange={selectedRow} disabled={selectedCards !== null && selectedCards !== undefined && selectedCards >= 5} />
                        </TableCell>
                    )}
                    {header.map((column: any) => (
                        <TableCell key={row[column.col]}>
                            {row[column.col]}
                        </TableCell>
                    ))}
                </TableRow>
            )}
        </>
    );
}