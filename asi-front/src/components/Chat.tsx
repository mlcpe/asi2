import { Button, Card, CardContent, CardHeader, FormControl, Input, InputLabel, MenuItem, Select, SelectChangeEvent, Typography } from "@mui/material";
import React, { MouseEvent, ReactNode, useEffect, useState } from "react";
import UserAPI from "../services/api/client/UserAPI";
import Gravatar from "react-gravatar";
import { toast } from "react-toastify";
import ChatApi from "../services/api/client/ChatApi";
import { ChatMessage } from "./Interfaces/InterfaceChatMessage";

import { User } from "../services/api/types/user";
import { io } from "socket.io-client";

export const socket = io("http://localhost:8080");

interface Props {
    currentUser: User;
    setCurrentUser: React.Dispatch<User>;
}

export default function Chat({ currentUser, setCurrentUser }: Props) {
    const [messages, setMessages] = React.useState<ChatMessage[]>([]);
    const [globalMessages, setGlobalMessages] = React.useState<ChatMessage[]>([]);
    const [currentMessage, setCurrentMessage] = React.useState<string>("");
    const [privateCurrentMessage, setPrivateCurrentMessage] = React.useState<string>("");

    const [myUser, setMyUser] = React.useState({} as any);
    const [users, setUsers] = React.useState<User[]>([]);

    const fetchDatas = async () => {
        const response = await UserAPI.users();
        if (response?.body?.data) {
            response.body.data.forEach((user: any) => {
                if (user.id !== Number(localStorage.getItem("token"))) setUsers((users: any) => [...users, user]);
            });
        }
    };

    const fetchUser = async () => {
        const response = await UserAPI.getLocalUser();
        console.log(response);
        if (response.type === "success") setMyUser(response.body.data);
    };

    const sendChatMessage = async (e: MouseEvent) => {
        e.preventDefault();

        const response = await ChatApi.sendMessage({
            senderId: currentUser.id,
            message: currentMessage,
            targetId: Number(localStorage.getItem("token")),
        });

        if (response?.status === 403) {
            toast.error("Error");
        } else {
            toast.success("Message sent !");
        }
    };

    const sendPrivateChatMessage = async (e: MouseEvent) => {
        e.preventDefault();
        const datas = {
            senderId: currentUser.id,
            message: privateCurrentMessage,
            targetId: Number(localStorage.getItem("token")),
        };

        console.log(datas)

        const response = await ChatApi.sendMessagePTP(datas, Number(localStorage.getItem("token")), currentUser.id);

        if (response?.status === 403) {
            toast.error("Error");
        } else {
            toast.success("Message sent !");
        }
    };

    React.useEffect(() => {
        fetchUser();
        fetchDatas();
    }, []);

    const handleChange = (event: SelectChangeEvent<any>, _child: ReactNode) => {
        setCurrentUser(event.target.value as any);
    };

    const [_isConnected, setIsConnected] = useState(socket.connected);

    useEffect(() => {
        socket.connect();
        socket.emit("join", localStorage.getItem("token"));
        
        function onConnect() {
            console.info("Connected to socket.io server");
            setIsConnected(true);
        }

        function onDisconnect() {
            console.info("Disconnected from socket.io server");
            setIsConnected(false);
        }

        function onChatNotificationEvent(value: ChatMessage) {
            console.info("ChatNotificationEvent", value);
            setGlobalMessages((previous) => [...previous, value]);
        }

        function onChatPrivateNotificationEvent(value: ChatMessage) {
            console.info("chatPrivateNotification", value);
            setMessages((previous) => [...previous, value]);
        }

        socket.on("connect", onConnect);
        socket.on("disconnect", onDisconnect);
        socket.on("chatPrivateNotification", onChatPrivateNotificationEvent);
        socket.on("chatNotification", onChatNotificationEvent);

        return () => {
            socket.off("connect", onConnect);
            socket.off("disconnect", onDisconnect);
            socket.off("chatPrivateNotification", onChatPrivateNotificationEvent);
            socket.off("chatNotification", onChatNotificationEvent);
        };
    }, []);

    const foundNameUser = (id: number) => {
        let name = "";
        users.forEach((user) => {
            if (user.id === id) name = user.login;
        });
        return name;
    };

    return (
        <>
            <Card>
                <CardHeader
                    title="Private"
                    subheader={
                        currentUser.login && (
                            <>
                                <Gravatar size={12} email={currentUser.email ?? "anonymous@abc.com"} /> {currentUser.login}
                            </>
                        )
                    }
                />
                <FormControl sx={{ minWidth: 120, width: "100%" }} size="small">
                    <InputLabel id="demo-simple-select-helper-label">User</InputLabel>
                    <Select labelId="demo-simple-select-helper-label" id="demo-simple-select-helper" value={currentUser} label="User" onChange={handleChange}>
                        <MenuItem value="Global">Global</MenuItem>
                        {users.map((user: any, k) => (
                            <MenuItem key={k} value={user}>
                                {user.login ?? "anonymous"}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
                {currentUser.login && (
                    <>
                        <CardContent sx={{ maxHeight: 200, overflow: "auto" }}>
                            {messages.map((message, k) => (
                                <div
                                    key={k}
                                    style={{
                                        textAlign:
                                            localStorage.getItem("token") && message.senderId && message.senderId.toString() === localStorage.getItem("token")
                                                ? "left"
                                                : "right",
                                    }}
                                >
                                    <Typography
                                        variant="body1"
                                        color={
                                            localStorage.getItem("token") && message.senderId && message.senderId.toString() === localStorage.getItem("token")
                                                ? "primary"
                                                : "secondary"
                                        }
                                    >
                                        {localStorage.getItem("token") && message.senderId && message.senderId.toString() === localStorage.getItem("token")
                                            ? foundNameUser(Number(message.targetId)) !== currentUser.login ? "" : foundNameUser(Number(message.targetId)) ?? "anonymous"
                                            : myUser.login ?? "anonymous"}
                                    </Typography>
                                    {message.senderId && message.senderId.toString() === localStorage.getItem("token") && foundNameUser(Number(message.targetId)) !== currentUser.login ? "" :
                                        <Typography
                                            variant="body2"
                                            color={
                                                localStorage.getItem("token") && message.senderId && message.senderId.toString() === localStorage.getItem("token")
                                                    ? "primary"
                                                    : "secondary"
                                            }
                                        >
                                            {message.message}
                                        </Typography>
                                    }
                                </div>
                            ))}
                        </CardContent>
                        <hr />
                        <Input
                            color="primary"
                            disabled={false}
                            size="small"
                            placeholder="Message"
                            fullWidth
                            onChange={(e) => setPrivateCurrentMessage(e.target.value)}
                        />
                        <Button variant="contained" color="primary" size="small" sx={{ width: "100%", borderRadius: 0 }} onClick={sendPrivateChatMessage}>
                            Send To Him
                        </Button>
                    </>
                )}
            </Card>
            <Card>
                <CardHeader title="Global" />
                <CardContent sx={{ maxHeight: 200, overflow: "auto" }}>
                    {globalMessages.map((message, k) => (
                        <div
                            key={k}
                            style={{
                                textAlign:
                                    localStorage.getItem("token") && message.targetId && message.targetId.toString() !== localStorage.getItem("token")
                                        ? "left"
                                        : "right",
                            }}
                        >
                            <Typography
                                variant="body1"
                                color={
                                    localStorage.getItem("token") && message.targetId && message.targetId.toString() !== localStorage.getItem("token")
                                        ? "primary"
                                        : "secondary"
                                }
                            >
                                {localStorage.getItem("token") && message.targetId && message.targetId.toString() !== localStorage.getItem("token")
                                    ? foundNameUser(Number(message.targetId)) ?? "anonymous"
                                    : myUser.login ?? "anonymous"}
                            </Typography>
                            <Typography
                                variant="body2"
                                color={
                                    localStorage.getItem("token") && message.targetId && message.targetId.toString() !== localStorage.getItem("token")
                                        ? "primary"
                                        : "secondary"
                                }
                            >
                                {message.message}
                            </Typography>
                        </div>
                    ))}
                </CardContent>
                <hr />
                <Input color="primary" disabled={false} size="small" placeholder="Message" fullWidth onChange={(e) => setCurrentMessage(e.target.value)} />
                <Button variant="contained" color="primary" size="small" sx={{ width: "100%", borderRadius: 0 }} onClick={sendChatMessage}>
                    Send Global
                </Button>
            </Card>
        </>
    );
}
