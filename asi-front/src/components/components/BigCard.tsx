import {Button, Card, CardContent, CardMedia, Chip, Grid, Stack, TextareaAutosize, Typography} from "@mui/material";
import {CardProps} from "./LittleCard";
import {InterfaceCard} from "../Interfaces/InterfaceCard";
import {sendGameMessage} from "../../pages/play/Play";
import {useAppSelector, useGameState, useOwnGameId} from "../../services/store";
import {selectGameState} from "../../services/store/gameSlice";
import {getAttackerAndTargetMapper} from "../../serverGameLib/tools";
import {StartedRoomState} from "../../services/api/types/room";


export interface ButtonProps extends InterfaceCard {

}


export interface Props extends CardProps {
    actionType: "SELL" | "BUY" | "GAME" | "NONE"
}


function CardButton(props: Props) {
    const gameState = useGameState() as StartedRoomState;
    const ownGameId = useOwnGameId();
    const targetId = gameState[ownGameId].selectedTargetCard;

    function SellButton(card: ButtonProps) {
        function onClick() {
            console.log("sell");
            // StoreAPI.sell() TODO
        }

        return <Button variant="contained" color="primary" sx={{width: "100%"}} onClick={onClick}>
            Actual Value : {card.price}€
        </Button>
    }

    function BuyButton(card: ButtonProps) {
        function onClick() {
            console.log("buy");
            // StoreAPI.buy() TODO
        }

        return <Button variant="contained" color="primary" sx={{width: "100%"}} onClick={onClick}>
            Actual Value : {card.price}€
        </Button>
    }

    function AttackButton(card: ButtonProps) {
        function onClick() {
            console.log("attack");

            targetId &&
            sendGameMessage("attack",
                {
                    attackerId: card.id,
                    targetId
                }
            )

        }

        return <Button variant="contained" color="error" sx={{width: "100%"}} onClick={onClick} disabled={!targetId}>
            Attack !!
        </Button>
    }

    switch (props.actionType) {

        case "SELL":
            return <SellButton {...props.card}/>
        case "BUY":
            return <BuyButton {...props.card}/>
        case "GAME":
            return <AttackButton {...props.card} />
        default:
            return <></>
    }
}

export default function BigCard({card, actionType}: Props) { // TODO CONTINUER

    return (
        <Grid item sx={{width: "100%", height: "100%"}}>
            <Card sx={{width: "100%", height: "100%"}}>
                <CardMedia sx={{height: 100}} image={card.imgUrl} title="green iguana"/>
                <CardContent>
                    <Typography variant="body1">
                        {card.name}
                    </Typography>
                    <Typography variant="body2" style={{fontSize: 10, marginBottom: 4}}>
                        {card.family}
                    </Typography>
                    <TextareaAutosize aria-label="minimum height" minRows={2} value={card.description}
                                      style={{width: "100%"}}/>
                    <Stack direction="row" spacing={1} sx={{marginTop: 1}}>
                        <Chip label={card.energy} color="primary"/>
                        <Chip label={card.hp} color="success"/>
                        <Chip label={card.defence} color="info"/>
                        <Chip label={card.attack} color="error"/>
                    </Stack>
                    <Stack direction="row" spacing={1} sx={{marginTop: 2}}>
                        <CardButton card={card} actionType={actionType}/>
                    </Stack>
                </CardContent>
            </Card>
        </Grid>
    );
}
