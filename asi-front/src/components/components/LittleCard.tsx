import {Card, CardContent, CardMedia, Chip, Grid, Stack, Typography} from "@mui/material";
import {InterfaceCard} from "../Interfaces/InterfaceCard";
import React from "react";
import {useAppDispatch} from "../../services/store";
import {sendGameMessage} from "../../pages/play/Play";
export interface CardProps {
    card: InterfaceCard,
}

export default function LittleCard(card: InterfaceCard) {
    const dispatch = useAppDispatch();
    function onClick() {
        console.log("onClick selectCard", card.id);
        sendGameMessage("selectCard", card.id);
    }
    return (
        <Grid item xs={3} onClick={onClick}
              sx={{display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center"}}>
            <Card sx={{width: "90%"}}>
                <CardMedia sx={{height: 100}} image={card.imgUrl} title="green iguana"/>
                <CardContent>
                    <Typography variant="body1">
                        {card.name}
                    </Typography>
                    <Stack direction="row" spacing={1}>
                        <Chip label={card.energy} color="primary"/>
                        <Chip label={card.hp} color="success"/>
                    </Stack>
                </CardContent>
            </Card>
        </Grid>
    );
}
