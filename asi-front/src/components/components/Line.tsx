import {Box, Grid, Typography} from "@mui/material";
import Gravatar from "react-gravatar";
import LinearProgress from "@mui/material/LinearProgress";
import BigCard from "./BigCard";
import {StartedRoomState} from "../../services/api/types/room";
import React from "react";
import LittleCard from "./LittleCard";
import {useGameState} from "../../services/store";

interface Props {
    gameId: "user1" | "user2"
    canAttack: boolean
}

export default function GameUserLine({gameId, canAttack}: Props) {
    const gameState = useGameState<StartedRoomState>()

    const user = gameState[gameId]
    console.info("gameId", gameId)

    const attackerCardId = user.selectedAttackerCard

    const selectedAttackerCard = !!attackerCardId && user.cards[attackerCardId]
    const actionPoints = Math.min(user.action_points * 10, 100)
    return (
        <Grid direction="row" container item xs={5} sx={{height: "100%", width: "100%"}}>

            <Grid container direction="column" alignItems="center" justifyContent="center" item xs={2}
                  sx={{width: "100%", height: "100%"}}>
                <Gravatar email={user.email ?? "anonymous@mail.net"}/>
                <Typography variant="body1" sx={{marginTop: 1}}>
                    {user.login ?? "anonymous"}
                </Typography>
                <Box sx={{width: "75%", marginTop: 1}}>
                    {actionPoints/10}/{10}<LinearProgress variant="determinate" value={actionPoints}/>
                </Box>
            </Grid>

            <Grid item container xs={7} sx={{paddingY: 2, paddingRight: 3, width: "100%", height: "100%"}}>
                {
                    !!user.cards && Object.values(user.cards).map(LittleCard)
                }
            </Grid>

            <Grid item xs={3} sx={{paddingY: 1, width: "100%", height: "100%"}}>
                {
                    selectedAttackerCard ?
                        <BigCard actionType={canAttack ? "GAME" : "NONE"} card={selectedAttackerCard}/>
                        :
                        <p>Pas de carte sélectionné</p>
                }

            </Grid>

        </Grid>
    );
}
