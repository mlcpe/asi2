import { TextField } from "@mui/material";

export default function InputLabel({ label, type, value, keyData, setValue } : { label: string, type: string, value: string, keyData: string; setValue: Function }) {
    return <TextField id={label} label={label} type={type} value={value} onChange={(event) => setValue(keyData, event.target.value)} variant="standard" color="secondary" />;
}
