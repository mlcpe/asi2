import { Fab } from "@mui/material";
import React from "react";

export default function MenuLink({ color, label, icon, link }: { color: any; label: string; icon: any, link: string }) {
    return (
        <a href={link}>
            <Fab variant="extended" color={color} className="biggerButtons">
                {icon && React.createElement(icon)} {label} 
            </Fab>
        </a>
    );
}
