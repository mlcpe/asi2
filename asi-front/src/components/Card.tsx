import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import BoltIcon from "@mui/icons-material/Bolt";
import FavoriteIcon from "@mui/icons-material/Favorite";
import { InterfaceCard } from "./Interfaces/InterfaceCard";

export default function Cards({
    data,
}: {
    data: InterfaceCard;
}) {
    return (
        <>
            {data && (
                <Card>
                    <CardContent sx={{ display: "flex", alignItems: "center", justifyContent: "space-between" }}>
                        <Typography gutterBottom variant="body2" component="div" sx={{ display: "flex", alignItems: "center", gap: 1}}>
                            <BoltIcon />
                            {data.energy}
                        </Typography>

                        <Typography gutterBottom variant="body1" component="div">
                            {data.name}
                        </Typography>

                        <Typography gutterBottom variant="body2" component="div" sx={{ display: "flex", alignItems: "center", gap: 1 }}>
                            {data.hp}
                            <FavoriteIcon />
                        </Typography>
                    </CardContent>
                    <CardMedia
                        component="img"
                        height="194"
                        image={data.imgUrl ?? "https://prod-printler-front-as.azurewebsites.net/media/photo/152462.jpg?mode=crop&width=425&height=600&rnd=0.0.1"}
                        alt={data.name}
                    />
                    <CardContent>
                        <Typography variant="body2" color="text.secondary">
                            {data.description}
                        </Typography>
                    </CardContent>
                </Card>
            )}
        </>
    );
}
