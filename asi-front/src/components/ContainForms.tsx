export default function ContainForms({ children, title, submitMethod } : { children: JSX.Element[] , title: string, submitMethod?: (e: React.FormEvent<HTMLFormElement>) => void}) {
    return (
        <div className="containGeneral">
            <div className="contain">
                <span className="absoluteTitle">{title}</span>
                <form onSubmit={submitMethod} className="formContain">
                    {children}
                </form>
            </div>
        </div>
    );
}