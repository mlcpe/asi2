import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import UserAPI from "../services/api/client/UserAPI";
import { InterfaceData } from "./Interfaces/InterfaceData";
import Gravatar from "react-gravatar";

export default function NavBar() {
    const [data, setData] = React.useState<InterfaceData>({});
    const WhereAmI = () => {
        const path = window.location.pathname;
        switch (path) {
            case "/":
                return "Home";
            case "/menu":
                return "Menu";
            case "/sell":
                return "Sell";
            case "/purchase":
                return "Buy";
            case "/page404":
                return "404";
            default:
                return "Home";
        }
    };

    React.useEffect(() => {
        const fetchDatas = async () => {
            const response = await UserAPI.getLocalUser();
            console.log(response);
            if (response?.body?.data) setData(response.body.data);
        };
        fetchDatas();
    }, []);

    return (
        <AppBar position="static" color="primary" className="fixedNavBar">
            <Container>
                <Toolbar sx={{ display: "flex", justifyContent: "space-between", width: "100%" }}>
                    <Box sx={{ flexGrow: 1, display: "flex", justifyContent: "start" }}>
                        <Typography
                            noWrap
                            sx={{
                                fontFamily: "monospace",
                                letterSpacing: ".2rem",
                            }}
                        >
                            {data?.account} €
                        </Typography>
                    </Box>

                    <Box sx={{ flexGrow: 1, display: "flex", justifyContent: "center" }}>
                        <a href="/card">
                            <Typography
                                noWrap
                                sx={{
                                    fontSize: "1.5rem",
                                    fontFamily: "monospace",
                                    fontWeight: 700,
                                    letterSpacing: ".3rem",
                                    textTransform: "uppercase",
                                    textDecoration: "none",
                                }}
                            >
                                {WhereAmI()}
                            </Typography>
                        </a>
                    </Box>

                    <Box sx={{ flexGrow: 1, display: "flex", alignItems: "center", justifyContent: "end" }}>
                        <IconButton>
                            <Gravatar email={data.email ?? "unkown@unkown.com"} />
                        </IconButton>
                        <Typography
                            noWrap
                            sx={{
                                letterSpacing: ".3rem",
                            }}
                        >
                            {data?.login || "Unknown"}
                        </Typography>
                    </Box>
                </Toolbar>
            </Container>
        </AppBar>
    );
}
