import React from "react";
import { Button } from "@mui/material";

export default function Buttons({ type, label, color, icon, method }: { type: any, label: string; color: any; icon?: any, method?: () => void | Function }) {
    return (
        <Button type={type} variant="outlined" color={color} startIcon={icon && React.createElement(icon)} onClick={() => method}>
            {label}
        </Button>
    );
}