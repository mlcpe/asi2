import { Alert, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from "@mui/material";
import TableLine from "./TableLine";

export default function TableWrapper({ header, data, selectedRow, selectRow, isForSelect = false, selectedCards }: { header: any; data: any, selectedRow?: number, selectRow: any, isForSelect?: boolean, selectedCards?: number }) {
    return (
        <>
            {data && data.length > 0 ? (
                <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                {header.map((column: any) => (
                                    <TableCell key={column.col}>
                                        <Typography variant="subtitle2" className="text-[11px] font-bold uppercase text-blue-gray-400">
                                            {column.id}
                                        </Typography>
                                    </TableCell>
                                ))}
                                {isForSelect && (
                                    <TableCell>
                                    </TableCell>
                                )}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {data.map((row: any) => (
                                <TableLine selected={selectedRow} selectedRow={selectRow} indexNumber={row.id} key={row.id} row={row} header={header} isForSelect={isForSelect} selectedCards={selectedCards} />
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            ) : (
                <Alert severity="warning">No card</Alert>
            )}
        </>
    );
}
