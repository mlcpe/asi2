export interface ChatMessage {
    senderId: number;
    targetId?: number;
    message: string;
}