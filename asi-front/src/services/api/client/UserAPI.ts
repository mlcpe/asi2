import APIClient from "../APIClient";
import {User} from "../types/user";

type DefaultType = User

export default class UserAPI extends APIClient {
    public static async users() {
        return this.get<User>(`/users`)
    }

    public static async getLocalUser() {
        return this.getUser(Number(localStorage.getItem("token") || ""))
    }

    public static async getUser(id: User["id"]) {
        return this.get<DefaultType>(`/user/${id}`)
    }


    public static async editUser(id: User["id"], data: Partial<User>) {
        return this.put<DefaultType>(`/user/${id}`, data)
    }

    public static async deleteUser(id: DefaultType["id"]) {
        return this.delete(`/user/${id}`)
    }
}