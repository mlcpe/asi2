import APIClient from "../APIClient";

export default class LoginAPI extends APIClient {    
    public static async auth(data:{}) {
        return this.post<any>("/auth", data)
    } 
    
    public static async account(data:{}) {
        return this.post("/user", data)
    } 
}