
import { ChatMessage } from "../../../components/Interfaces/InterfaceChatMessage";
import APIClient from "../APIClient";

export default class ChatAPI extends APIClient {
    public static async sendMessage(data: ChatMessage) {
        return this.post(`/api/chat/send`, data);
    }
    
    public static async sendMessagePTP(data: ChatMessage, sender: number, receiver: number) {
        return this.post(`/api/chat/send-peer-to-peer/${sender}/${receiver}`, data);
    }
}