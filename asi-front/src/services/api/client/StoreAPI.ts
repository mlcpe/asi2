import APIClient from "../APIClient";
import {InterfaceCard} from "../../../components/Interfaces/InterfaceCard";



export default class StoreAPI extends APIClient {    
    public static async sell(data:{}) {
        return this.post("/store/sell", data)
    } 
    
    public static async buy(data:{}) {
        return this.post("/store/buy", data)
    } 
    
    public static async transaction() {
        return this.get("/store/transaction")
    } 
}