import APIClient from "../APIClient";
import {CardModel} from "../types/room";
import {InterfaceCard} from "../../../components/Interfaces/InterfaceCard";

type DefaultType = InterfaceCard

export default class CardAPI extends APIClient {
    public static async getCards() {
        return this.get<DefaultType[]>(`/cards`)
    }

    public static async getCardsSells() {
        return this.get<DefaultType>(`/cards_to_sell`)
    }

    public static async newCard(data: {}) {
        return this.post<DefaultType>(`/card`, data)
    }

    public static async getCard(id: number) {
        return this.get<DefaultType>(`/card/${id}`)
    }

    public static async editCard(id: number, data:{}) {
        return this.put(`/card/${id}`, data)
    }

    public static async deleteCard(id: number) {
        return this.delete(`/card/${id}`)
    }
}