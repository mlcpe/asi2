import axios, {AxiosResponse} from "axios";
// import createAuthRefreshInterceptor from "axios-auth-refresh";

const BASE_URL = "http://localhost:8080/api"

type APIData<BodyDataType extends any = any> = {
    status: number,
    data: BodyDataType
}

// const refreshAuthLogic = (failedRequest: { response: { config: { headers: { [x: string]: string; }; }; }; }) =>
//     axios.post(BASE_URL + '/user/refresh-token', {
//         "refreshToken": localStorage.getItem('uhohuvwrsdqgdkqjp')
//     }).then((tokenRefreshResponse) => {
//         localStorage.setItem('wrgqhdpqlw', tokenRefreshResponse.data.accessToken);
//         localStorage.setItem('uhohuvwrsdqgdkqjp', tokenRefreshResponse.data.refreshToken);
//         failedRequest.response.config.headers['Authorization'] = 'Bearer ' + tokenRefreshResponse.data.accessToken;
//         return Promise.resolve();
//     });

// createAuthRefreshInterceptor(axios, refreshAuthLogic);

type APIResponse<Rtype extends any> = {
    type: "success",
    status: number,
    body: APIData<Rtype>
} | {
    type: "error",
    status: number,
    body: APIData
}

export default class APIClient {
    private static async tryRequest<Rtype extends any>(req: Promise<AxiosResponse<any, any>>): Promise<APIResponse<Rtype>> {
        try {
            const result = await req;
            return {
                type: "success",
                status: result.status,
                body: result
            };
        } catch (e: any) {
            return {
                type: "error",
                status: e.status,
                body: e
            }
        }
    }

    protected static async get<Rtype extends any>(path: string) {
        //const headers = await this.getAuthHeader()
        const headers: any = {}
        return this.tryRequest<Rtype>(
            axios.get(BASE_URL + path, {headers})
        );
    }

    protected static async getArgs<Rtype extends any>(path: string, data: any) {
        //const headers = await this.getAuthHeader()
        const headers: any = {}
        return this.tryRequest<Rtype>(
            axios.get(BASE_URL + path, {headers: headers, data: data})
        );
    }

    protected static async delete<Rtype extends any = any>(path: string) {
        //const headers = await this.getAuthHeader()
        const headers: any = {}
        return this.tryRequest<Rtype>(
            axios.delete(BASE_URL + path, {headers})
        );
    }

    protected static async post<Rtype extends any>(path: string, data: any) {
        //const headers: any = await this.getAuthHeader()
        const headers: any = {}
        headers["Content-Type"] = "application/json"
        return this.tryRequest<Rtype>(
            axios.post(BASE_URL + path, data, {headers})
        )
    }

    protected static async put<Rtype extends any>(path: string, data: any) {
        //const headers: any = await this.getAuthHeader()
        const headers: any = {}
        headers["Content-Type"] = "application/json"
        return this.tryRequest<Rtype>(
            axios.put(BASE_URL + path, data, {headers})
        )
    }

    protected static async post_sign(path: string, data: any) {
        const headers: any = {}
        headers["Content-Type"] = "application/json"
        return this.tryRequest(
            axios.post(BASE_URL + path, data, {headers})
        )
    }

    private static async getAuthHeader() {
        const token = await localStorage.getItem('wrgqhdpqlw')
        if (!token)
            return {}
        return {"Authorization": `Bearer ${token}`}
    }
}