import {CardModel, RealRoomState} from "./room";

type ErrorGameTypes =
    "CANNOT_START" |
    "CANNOT_ATTACK" |
    "NOT_ENOUGH_POINT" |
    "INVALID_TARGET" |
    "INVALID_ATTACKER" |
    "INVALID_CARD"

type CardId = CardModel["id"]

interface GameMessageType {
    action: string
    payload?: unknown
}

export interface GameError<ErrorType extends ErrorGameTypes = ErrorGameTypes, Payload extends unknown = unknown> extends GameMessageType {
    action: ErrorType
    payload?: Payload
}

export interface GameAction<ActionType extends string, Payload extends unknown | undefined = undefined> extends GameMessageType {
    action: ActionType
    payload: Payload
}

export type ActionStart = GameAction<"start">

export type ActionAttack = GameAction<
    "attack",
    { attackerId: CardId, targetId: CardId }
>


export type ActionEndTurn = GameAction<"endTurn">

export type ActionRefresh = GameAction<"refresh">


export type ActionJoin = GameAction<
    "join",
    Pick<RealRoomState, "password">
>

export type ActionChooseCard = GameAction<
    "chooseCard",
    CardId[]
>

export type ActionSelectCard = GameAction<"selectCard", CardId>


export type GameMessage =
    ActionStart |
    ActionAttack |
    ActionEndTurn |
    ActionRefresh |
    ActionJoin |
    ActionChooseCard |
    ActionSelectCard;