export interface Message {
    senderId: number;
    message: string;
    targetId: number;
}