import {User} from "./user";
import {InterfaceCard} from "../../../components/Interfaces/InterfaceCard";

type CardId = CardModel["id"]
type UserId = User["id"]

export interface Room {
    state: "WAITING" | "STARTED" | "FINISHED"
    roomId: string;
    password: string;
    game: GameItr
    user1: UserStatus
    user2?: UserStatus
}

export interface UserStatus extends Pick<User, "id" | "login" | "email"> { // | "surName"
    cards: Record<CardId, CardModel>
    selectedAttackerCard: CardId | undefined
    selectedTargetCard: CardId | undefined
    action_points: number
}

export interface UserStatusStartItr extends UserStatus {
    action_points: 0
}

export interface GameItr {
    turn?: "user1" | "user2"
    winner: "user1" | "user2" | undefined
    round: number
}

export interface GameWaitingItr extends GameItr {
}

export interface GameStartedItr extends GameItr {
    turn: "user1" | "user2"
}

export interface GameFinishedItr extends GameItr {
    turn:  "user1" | "user2"
    winner: "user1" | "user2"
}

export type CardModel = InterfaceCard // Pick<InterfaceCard, "id" | "attack" | "defence" | "energy" | "hp">


export interface NoRoomState {
    state: "NO_ROOM"

}

export interface WaitingRoomState {
    state: "WAITING"
    roomId: string;
    password: string;
    game?: GameWaitingItr
    user1: UserStatus
    user2?: UserStatus
}

export interface StartedRoomState {
    state: "STARTED"
    roomId: string;
    password: string;
    game: GameStartedItr
    user1: UserStatus
    user2: UserStatus
}

export interface FinishedRoomState {
    state: "FINISHED"
    roomId: string;
    password: string;
    game: GameFinishedItr
    user1: UserStatus
    user2: UserStatus
}

export type RealRoomState =
    WaitingRoomState |
    FinishedRoomState |
    StartedRoomState

