export interface User {
    id: number,
    login: string,
    pwd: string,
    account: number,
    lastName: string | null,
    surName: string | null,
    email: string,
}