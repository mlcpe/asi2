import {configureStore} from "@reduxjs/toolkit";
import {TypedUseSelectorHook, useDispatch, useSelector} from "react-redux";
import gameReducer, {selectGameState, selectOwnGameId, selectTargetGameId,} from "./gameSlice";
import {NoRoomState, RealRoomState} from "../api/types/room";

/** Configuration du store d'états **/

const store = configureStore({ // Trouver un jour le moyen d'inférer correctement le type de RootState entre L'état de l'api et du Store
    reducer: {
        game: gameReducer,
    },
});


/*
store.subscribe(() => {
    saveAuthState(store.getState().auth)
})
*/

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export const useAppDispatch = () => useDispatch<AppDispatch>();

export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
export const useGameState = <T extends NoRoomState | RealRoomState>() => useAppSelector(selectGameState).gameState as T;
export const useOwnGameId = () => useAppSelector(selectOwnGameId) as unknown as "user1" | "user2"
export const useTargetGameId = () => useAppSelector(selectTargetGameId) as unknown as "user1" | "user2"

export default store;