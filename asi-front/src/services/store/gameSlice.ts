import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {NoRoomState, RealRoomState} from "../api/types/room";
import {RootState} from "./index";

interface InitGameStates {
    gameState: NoRoomState | RealRoomState,
    ownGameId?: "user1" | "user2",
    targetGameId?:  "user1" | "user2",
}

const initGameState:InitGameStates = {
    gameState: {
        state: "NO_ROOM",
    },
    ownGameId: undefined ,
    targetGameId: undefined ,
};


export const index = createSlice({
    name: "game",
    initialState: initGameState,
    reducers: {
        setGameState: (
            state,
            action: PayloadAction<RealRoomState>
        ) => {
            state.gameState = action.payload;
        },
        setUserId: (
            state,
            action: PayloadAction<"user1" | "user2">
        ) => {
            state.ownGameId = action.payload;
            state.targetGameId = action.payload === "user1" ? "user2" : "user1"
        },
        resetGameState: (state) => {
            state.gameState = initGameState["gameState"];
        }
    },
});


const selectGameState = (state: RootState) => state.game;
const selectOwnGameId = (state: RootState) => state.game.ownGameId;
const selectTargetGameId = (state: RootState) => state.game.targetGameId;

export {
    selectGameState,
    selectOwnGameId,
    selectTargetGameId
};

export const {
    setGameState,
    setUserId,
    resetGameState
} = index.actions;


export default index.reducer;


