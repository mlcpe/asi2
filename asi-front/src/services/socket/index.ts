import {Socket} from "socket.io";

const connections: Record<string, Socket> = {}

function makeId(length: number) {
    // Vielle fonction dégueulasse récupéré
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    let counter = 0;
    while (counter < length) {
        result += characters.charAt(Math.floor(Math.random() * characters.length));
        counter += 1;
    }
    return result;
}

function handleLifeCycle(socket: Socket) {
    const connectionId = makeId(42)

    connections[connectionId] = socket

    socket.on('disconnecting', () => {
        delete connections[connectionId]
    })
}

export {handleLifeCycle}