import { createTheme } from "@mui/material";

// Fonction pour générer une couleur aléatoire
const getRandomColor = () => {
  const letters = "0123456789ABCDEF";
  let color = "#";
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
};

// Crée une palette de couleurs aléatoires
const randomPalette = {
  primary: {
    main: getRandomColor(),
    light: getRandomColor(),
    dark: getRandomColor(),
    contrastText: getRandomColor(),
  },
  secondary: {
    main: getRandomColor(),
    light: getRandomColor(),
    dark: getRandomColor(),
    contrastText: getRandomColor(),
  },
  success: {
    main: getRandomColor(),
    light: getRandomColor(),
    dark: getRandomColor(),
    contrastText: getRandomColor(),
  },
  error: {
    main: getRandomColor(),
    light: getRandomColor(),
    dark: getRandomColor(),
    contrastText: getRandomColor(),
  },
};

export const Theme = createTheme({
  palette: randomPalette,
});
