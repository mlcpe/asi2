import {Server, Socket} from "socket.io";
import express from "express";
import http from "http";
import bodyParser from "body-parser";
import cors from "cors";

import CONFIG from "./config.json";
import {Message} from "./services/api/types/chat";
import {DefaultEventsMap} from "socket.io/dist/typed-events";
import {GameMessage} from "./services/api/types/roomMessage";
import * as gameHandler from "./serverGameLib";
import {ActionHandlerParams} from "./serverGameLib";
import {generateRoomId} from "./serverGameLib/tools";

const app = express();

app.use(cors({credentials: false}));

const server = http.createServer(app);

const ioServer = new Server(server);

app.use(bodyParser.json());

export type SocketType = Socket<DefaultEventsMap, DefaultEventsMap>


const userSockets = new Map<number, { id: number; socket: SocketType }>();


app.post("/api/chat/send/", async (req, res) => {
    const message = req.body as Message;
    console.info("send message api/chat/send/", message);
    ioServer.in("chat").emit("chatNotification", message);
    return res;
});

app.post("/api/chat/send-peer-to-peer/:sender/:receiver", async (req, res) => {
    const message = req.body as Message;
    const sender = req.params.sender;
    const receiver = req.params.receiver;

    console.info("send peer-to-peer message", message, "from", sender, "to", receiver);
    ioServer.to(receiver).emit("chatPrivateNotification", message);
    ioServer.to(sender).emit("chatPrivateNotification", message);
    return res;
});


app.use(express.static(CONFIG.static));

ioServer.on("connection", (socket) => {
    console.log("a user connected");
    socket.join("chat");

    let userId: number | null = null;
    let roomPassword: string | undefined = undefined; // TODO: different roomPassword and roomId

    socket.on("userConnect", (newUserId:number) => {
        userId = Number(newUserId);

        userSockets.set(userId, {id: userId, socket});
        console.log("userConnect", userId);
    });

    socket.on("join", (idUser: string) => {
        console.log("join room", idUser);
        socket.join(idUser);
    });


    socket.on("clientGameEvent", async (gameMessage: GameMessage) => {
        console.info("clientGameEvent", gameMessage);

        if (userId === null) {
            console.error("clientGameEvent userId is null")
            return;
        }

        const args: ActionHandlerParams = {socket, playerId: userId, payload: gameMessage.payload}

        await gameHandler[gameMessage.action](args as any);

    });

    socket.on("requestEvent", (data) => socket.emit("responseEvent", data));

});

server.listen(CONFIG.port);

export {app, ioServer};
