import {
    ActionAttack,
    ActionChooseCard,
    ActionJoin,
    ActionSelectCard,
    ActionStart,
    GameError,
    GameMessage
} from "../services/api/types/roomMessage";

import {RealRoomState, UserStatus, UserStatusStartItr} from "../services/api/types/room";
import {ioServer, SocketType} from "../server";

import {
    findPlayerInRoom,
    findRoomByPassword,
    findRoomByPlayer,
    getAttackerAndTargetMapper,
    getGameId,
    getOtherPlayer,
    randomUser,
    updateAttack,
    updateUserCard,
    updateUserNewTurnState,
    updateUserSelected
} from "./tools";
import UserAPI from "../services/api/client/UserAPI";

export type ActionHandlerParams<M extends GameMessage = GameMessage> = {
    socket: SocketType
    //room?: string
    playerId: number
    payload: M["payload"]
}

const gameRooms = new Map<string, RealRoomState>();

function updateRoomState(newRoomState: RealRoomState) {
    const {roomId} = newRoomState;
    gameRooms.set(roomId, newRoomState)
    ioServer.to(roomId).emit("serverGameRefresh", newRoomState);
}


export async function join({socket, playerId, payload}: ActionHandlerParams<ActionJoin>) {
    const {password} = payload;

    async function initUser(playerId: number) {
        const response = await UserAPI.getUser(playerId)

        if (response.type === "success") {
            return {
                ...response.body.data,
                cards: {},
                action_points: 0,
                selectedAttackerCard: undefined,
                selectedTargetCard: undefined,

            } as UserStatusStartItr
        } else {
            throw new Error("Cannot init user")
        }
    }

    // TODO: check if player is already in a room ; do the except
    // const existingRoom = findRoomByPlayer(playerId);

    // if (existingRoom) {
    //     res.status(403).json({ error: "Le joueur est déjà dans une salle." });
    //     return;
    // }
    const oldRoom = findRoomByPlayer(gameRooms, playerId)
    if (oldRoom) {
        socket.leave(oldRoom.roomId)
        gameRooms.delete(oldRoom.roomId)
    }

    let roomState = findRoomByPassword(gameRooms, password);

    if (!roomState) {
        console.log("create room");
        const roomId = password; //

        roomState = {
            state: "WAITING",
            roomId,
            password,
            user1: await initUser(playerId)
        };

        socket.join(roomState.roomId);

        gameRooms.set(roomId, roomState);
    } else if (!roomState.user2) {

        roomState = {
            ...roomState,
            user2: await initUser(playerId)
        };

        gameRooms.set(roomState.roomId, roomState);

        socket.join(roomState.roomId);
    } else if (roomState.user2.id !== playerId) return;

    updateRoomState(roomState);
}

export function start({socket, playerId, payload}: ActionHandlerParams<ActionStart>) {
    const roomState = findRoomByPlayer(gameRooms, playerId);

    if (
        !roomState ||
        roomState.state != "WAITING" ||
        roomState?.user2 === undefined) return;

    console.info("start condition passed")

    const playingUser = randomUser()

    const playingUserState = roomState[playingUser] as unknown as UserStatus

    updateRoomState({
        ...roomState,
        state: "STARTED",
        game: {
            round: 1,
            turn: playingUser,
            winner: undefined,
        },
        user2: roomState.user2, // Je suis obligé de faire ça sinon typescript me dit que user2 est undefined
        [playingUser]: updateUserNewTurnState(playingUserState, 1),
    });
}

export async function chooseCard({socket, playerId, payload}: ActionHandlerParams<ActionChooseCard>) {
    const roomState = findRoomByPlayer(gameRooms, playerId);

    console.info(roomState)

    if (
        !roomState ||
        roomState.state != "WAITING"
    ) return;
    console.info("chooseCard condition passed 1")

    const {user, playerRoomId} = findPlayerInRoom(roomState, playerId)

    if (!user || !playerRoomId) return;
    console.info(" chooseCard condition passed 2")
    let newRoomState;
    try {
        updateRoomState({
            ...roomState,
            [playerRoomId]: await updateUserCard(user, payload)
        });

    } catch (e) {

        const errorMessage: GameError<"INVALID_CARD"> = {
            action: "INVALID_CARD",
        }
        console.error(errorMessage, e)

        socket.emit("serverGameError", errorMessage)
        return;
    }

}


function notAnyAliveCard({cards}: Pick<UserStatus, "cards">) {
    return Object.values(cards).every(card => card.hp <= 0)
}

export function attack({socket, playerId, payload}: ActionHandlerParams<ActionAttack>) {
    const roomState = findRoomByPlayer(gameRooms, playerId);

    if (
        !roomState ||
        roomState.state != "STARTED"
    ) return;
    console.info("attack condition passed 1")

    const {userTargetId, userAttackerId} = getAttackerAndTargetMapper(roomState.game.turn);
    const userTarget = roomState[userTargetId];
    const userAttacker = roomState[userAttackerId];

    if (
        roomState[userAttackerId].id !== playerId
    ) return;
    console.info("attack condition passed 2")

    const {attackerId, targetId} = payload;

    const cardAttacker = roomState[userAttackerId].cards[attackerId]
    const cardTarget = roomState[userTargetId].cards[targetId]

    console.info("userAttackerId points", roomState[userAttackerId].action_points, cardAttacker.energy)

    if (
        !cardAttacker || !cardTarget || roomState[userAttackerId].action_points <= 0 || cardAttacker.hp <= 0
    ) return;
    console.info("attack condition passed 3")
    const [newUserAttacker, newCardTarget] = updateAttack(userAttacker, cardAttacker, cardTarget)

    const newUserTarget = {
        ...roomState[userTargetId],
        cards: {
            ...roomState[userTargetId].cards,
            [targetId]: newCardTarget
        }
    }
    const winner = notAnyAliveCard(newUserTarget) ? userAttackerId : undefined

    updateRoomState({
        ...roomState,
        state: !!winner ? "FINISHED" : "STARTED",
        game: {
            ...roomState.game,
            round: roomState.game.round,
            winner: winner,
        },

        [userAttackerId]: newUserAttacker,

        [userTargetId]: newUserTarget
    } as RealRoomState)

}

export function endTurn({socket, playerId, payload}: ActionHandlerParams<ActionStart>) {
    const roomState = findRoomByPlayer(gameRooms, playerId);

    if (
        !roomState ||
        roomState.state != "STARTED"
    ) return;
    const playingUser = roomState.game.turn


    if (roomState[playingUser].id != playerId) return;

    const nextPlayingUser = getOtherPlayer(playingUser)

    updateRoomState({
        ...roomState,
        game: {
            ...roomState.game,
            round: roomState.game.round + 1,
            turn: nextPlayingUser,
        },
        [nextPlayingUser]: updateUserNewTurnState(roomState[nextPlayingUser], roomState.game.round),
    })

}

export function selectCard({socket, playerId, payload}: ActionHandlerParams<ActionSelectCard>) {
    const roomState = findRoomByPlayer(gameRooms, playerId);
    console.info(roomState)
    if (
        !roomState ||
        roomState.state != "STARTED"
    ) return;

    const gameId = getGameId(roomState, playerId)

    let selectedCard: "selectedAttackerCard" | "selectedTargetCard" = !!roomState[gameId].cards[payload] ? "selectedAttackerCard" : "selectedTargetCard"


    updateRoomState({
            ...roomState,
            [gameId]: updateUserSelected(selectedCard, roomState[gameId], payload)
        }
    )
}

export function refresh({socket, playerId, payload}: ActionHandlerParams<ActionStart>) {
    //updateRoomStateToUser(socket, findRoomByPlayer(gameRooms, playerId))
}

