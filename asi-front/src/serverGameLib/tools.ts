import {CardModel, RealRoomState, UserStatus} from "../services/api/types/room";
import {SocketType} from "../server";
import {v4 as uuidv4} from "uuid";
import CardAPI from "../services/api/client/CardAPI";
import {ActionChooseCard} from "../services/api/types/roomMessage";

class InvalidCard extends Error {
    constructor(message: any) {
        super(message);
        this.name = 'BadRequestError';
    }
}


export function sendRefreshGameToUser(socket: SocketType, room: RealRoomState | undefined) {
    socket.emit("serverGameRefresh", room);
}

export function findCardById(cardModels: Record<CardModel["id"], CardModel>, cardIdToFind: number) {
    const cardIds = Array.from(Object.keys(cardModels));
    return cardIds.find(cardId => cardIdToFind === Number(cardId));
}

export function getAttackerAndTargetMapper(userAttackerId: "user1" | "user2") {
    type RType = {
        userAttackerId: "user1",
        userTargetId: "user2"
    } | {
        userAttackerId: "user2",
        userTargetId: "user1"
    };
    return {userAttackerId, userTargetId: userAttackerId === "user1" ? "user2" : "user1"} as RType
}

export function getGameId(room: RealRoomState, playerId: number) {
    return room.user1.id === playerId ? "user1" : "user2";
}

export function getOtherPlayer(playingUser: "user1" | "user2") {
    return playingUser == "user1" ? "user2" : "user1";
}

export function findRoomByPassword(gameRooms: Map<string, RealRoomState>, password: string) {
    const rooms = Array.from(gameRooms.values());
    return rooms.find((room) => room.password === password);
}

export function findRoomByPlayer(gameRooms: Map<string, RealRoomState>, playerId: number) {
    return Array.from(gameRooms.values())
        .find(
            room => room.user1.id === playerId || room.user2?.id === playerId
        );
}

export function findPlayerInRoom(room: RealRoomState, playerId: number) {
    if (room.user1.id === playerId) return {playerRoomId: "user1", user: room.user1};
    else if (room.user2?.id === playerId) return {playerRoomId: "user2", user: room.user2};
    return {playerRoomId: undefined, user: undefined};
}

export function updateUserNewTurnState(userState: UserStatus, round: number): UserStatus {
    return {
        ...userState,
        action_points: Math.min(userState.action_points + 1 + (Math.max(Math.floor(round) - (round % 2) -1,0)), 10)
    };
}

export function updateUserSelected(key: "selectedTargetCard" | "selectedAttackerCard", userState: UserStatus, id: CardModel["id"]): UserStatus {
    return {...userState, [key]: id};
}

export async function updateUserCard(userState: UserStatus, cardIds: ActionChooseCard["payload"]) {
    const cardModels: Record<string, CardModel> = {}

    for (const cardId of cardIds) {
        const {type, status, body} = await CardAPI.getCard(cardId)

        if (type === "error") {
            if (status >= 500)
                throw new Error("Server error")
            else
                throw new InvalidCard("Card not found")
        } else if (body.data.userId !== userState.id) {
            console.info(typeof body.data.userId, typeof userState.id)
            throw new InvalidCard(`User don't have this card ${body.data.userId} !== ${userState.id}`)
        } else {
            cardModels[body.data.id] = body.data;
        }
    }

    return {...userState, cards: cardModels};
}

export function randomUser() {
    return Math.random() > 0.5 ? "user1" : "user2";
}


export function updateAttack(
    userAttacker: UserStatus,
    cardAttacker: CardModel,
    cardTarget: CardModel,
): [UserStatus, CardModel] {


    return [
        {
            ...userAttacker,
            action_points: userAttacker.action_points - 1
        },
        {
            ...cardTarget,
            hp: Math.max(cardTarget.hp - cardAttacker.attack, 0)
        }
    ]
}

export function generateRoomId() {
    return uuidv4();
}
