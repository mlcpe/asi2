import React from "react";
import ReactDOM from "react-dom/client";
import {BrowserRouter, Navigate, Route, Routes} from "react-router-dom";

import "./assets/css/index.css";

import Login from "./pages/login/Login";
import Register from "./pages/register/Register";
import Menu from "./pages/Menu/Menu";
import Purchase from "./pages/purchase/Purchase";
import Sell from "./pages/sell/Sell";
import Page404 from "./pages/page404/Page404";
import {ThemeProvider} from "@mui/material";

import {Theme} from "./Theme"
import {ToastContainer} from "react-toastify";

import 'react-toastify/dist/ReactToastify.css';
import Play from "./pages/play/Play";
import store from "./services/store";
import {Provider} from "react-redux";

const root = ReactDOM.createRoot(document.getElementById("root") as HTMLElement);


root.render(
    <React.StrictMode>
        <ThemeProvider theme={Theme}>
            <Provider store={store}>

                <BrowserRouter>
                    <Routes>
                        <Route path="/"
                               element={localStorage.getItem("token") ? <Navigate to="/card"/> :
                                   <Navigate to="/login"/>}/>
                        <Route path="/login" element={<Login/>}/>
                        <Route path="/addUser" element={<Register/>}/>
                        <Route path="/card"
                               element={localStorage.getItem("token") ? <Menu/> : <Navigate to="/login"/>}/>
                        <Route path="/sell"
                               element={localStorage.getItem("token") ? <Sell/> : <Navigate to="/login"/>}/>
                        <Route path="/purchase"
                               element={localStorage.getItem("token") ? <Purchase/> : <Navigate to="/login"/>}/>
                        <Route path="/play"
                               element={localStorage.getItem("token") ? <Play/> : <Navigate to="/login"/>}/>
                        <Route path="*" element={localStorage.getItem("token") ? <Page404/> : <Navigate to="/login"/>}/>
                    </Routes>
                    <ToastContainer
                        position="bottom-right"
                        autoClose={5000}
                        hideProgressBar={false}
                        newestOnTop={false}
                        closeOnClick
                        rtl={false}
                        pauseOnFocusLoss
                        draggable
                        pauseOnHover
                        theme="light"
                    />
                </BrowserRouter>
            </Provider>
        </ThemeProvider>
    </React.StrictMode>
);
