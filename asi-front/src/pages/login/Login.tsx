import React from "react";
import LoginIcon from "@mui/icons-material/Login";

import Button from "../../components/Button";
import ContainForms from "../../components/ContainForms";
import InputLabel from "../../components/InputLabel";
import LoginAPI from "../../services/api/client/LoginAPI";
import { toast } from "react-toastify";

export default function Login() {
    const [data, setData] = React.useState<{login: string, password: string}>({
        login: "",
        password: "",
    });

    const handleChangeData = (keyData: string, value: string) => {
        setData((prevState) => ({ ...prevState, [keyData]: value }));
    }

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const datas = {
            username: data.login,
            password: data.password,
        };
        const response = await LoginAPI.auth(datas);
        // @ts-ignore
        if (response?.body?.response?.status === 403) {
            toast.error('Bad credentials');
        } else {
            toast.success('You are connected !');
            localStorage.setItem("token", response?.body?.data);
            window.location.href = "/card";
        }
    }

    return (
        <ContainForms title="Login" submitMethod={handleSubmit}>
            <InputLabel label="Surname" type="text" value={data.login} keyData="login" setValue={handleChangeData} />
            <InputLabel label="Password" type="password" value={data.password} keyData="password" setValue={handleChangeData} />

            <Button type="submit" icon={LoginIcon} label="Login" color="success" />
            <Button type="button" label="Cancel" color="error" />
            <a href="/addUser">Register</a>
        </ContainForms>
    );
}
