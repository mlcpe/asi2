import { Grid } from "@mui/material";
import NavBar from "../../components/NavBar";

import Box from '@mui/material/Box';
import TableWrapper from "../../components/Table";
import { headerTable } from "./Header";
import Cards from "../../components/Card";
import Buttons from "../../components/Button";
import React from "react";
import CardAPI from "../../services/api/client/CardAPI";
import StoreAPI from "../../services/api/client/StoreAPI";
import { InterfaceCard } from "../../components/Interfaces/InterfaceCard";
import { toast } from "react-toastify";

export default function Purchase() {
    const [selected, setSelected] = React.useState(-1);
    const [cards, setCards] = React.useState<InterfaceCard[]>([]);
    
    const fetchDatas = async () => {
        const response = await CardAPI.getCardsSells();
        console.log(response);
        if (response?.body?.data) setCards(response.body.data);
    }

    React.useEffect(() => {
        fetchDatas();
    }, []);

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (selected === -1) return console.log("No card selected");
        const datas = {
            "user_id": Number(localStorage.getItem("token")),
            "card_id": cards[selected].id
        }
        const response = await StoreAPI.buy(datas);

        if (response?.body?.data) {
            toast.success('You have buy this card !');   
            setSelected(-1)
            fetchDatas();
        }
        else
            toast.error('You have not buy this card !');
    }

    return (
        <>
            <NavBar />
            <Box sx={{ flexGrow: 1, paddingX: 4, marginTop: 2 }}>
                <h1>Market</h1>
                <Grid container spacing={2}>
                    <Grid item xs={8}>
                        <TableWrapper selectedRow={selected} data={cards} header={headerTable} selectRow={setSelected} />
                    </Grid>
                    <Grid item xs={4} sx={{ display: "flex", flexDirection: "column", justifyContent: "flex-start", gap: 2}}>
                        <Cards data={cards[selected]} />
                        
                        {selected !== -1 && <form onSubmit={handleSubmit}><Buttons type="submit" label={`Buy : ${cards[selected].price}€`} color="success"  /></form>}
                    </Grid>
                </Grid>
            </Box>
        </>
    );
}