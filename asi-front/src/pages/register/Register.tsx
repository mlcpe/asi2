import React from "react";
import AddIcon from '@mui/icons-material/Add';

import Buttons from "../../components/Button";
import ContainForms from "../../components/ContainForms";
import InputLabel from "../../components/InputLabel";
import { toast } from "react-toastify";
import LoginAPI from "../../services/api/client/LoginAPI";

export default function Register() {
    const [data, setData] = React.useState<{name: string, surname: string, password: string, repassword: string, login: string, email: string}>({
        name: "",
        surname: "",
        password: "",
        repassword: "",
        login: "",
        email: "",
    });

    const handleChangeData = (keyData: string, value: string) => {
        setData((prevState) => ({ ...prevState, [keyData]: value }));
    }

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (data.password !== data.repassword) {
            toast.warning('Password and Re-Password are not the same');
            return;
        }
        const datas = {
            lastName: data.name,
            surName: data.surname,
            pwd: data.password,
            login: data.login,
            email: data.email,
        };
        const response = await LoginAPI.account(datas);

        console.log("response",response.body)
        // @ts-ignore
        if (response?.body?.response?.status === 403) {
            toast.error('Bad credentials');
        } else {
            toast.success('You have create your account !');
            window.location.href = "/login";
        }
    }

    return (
        <ContainForms title="UserForm" submitMethod={handleSubmit}>
            <InputLabel label="Login" type="text" value={data.login} keyData="login" setValue={handleChangeData} />
            <InputLabel label="Email" type="email" value={data.email} keyData="email" setValue={handleChangeData} />
            <InputLabel label="Name" type="text" value={data.name} keyData="name" setValue={handleChangeData} />
            <InputLabel label="Surname" type="text" value={data.surname} keyData="surname" setValue={handleChangeData} />
            <InputLabel label="Password" type="password" value={data.password} keyData="password" setValue={handleChangeData} />
            <InputLabel label="Re-Password" type="password" value={data.repassword} keyData="repassword" setValue={handleChangeData} />

            <Buttons type="submit" icon={AddIcon} label="Ok" color="success" />
            <Buttons type="button" label="Cancel" color="error" />
            <a href="/login">Log in</a>
        </ContainForms>
    );
}
