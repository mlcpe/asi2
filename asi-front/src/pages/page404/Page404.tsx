import Buttons from "../../components/Button"
import ContainForms from "../../components/ContainForms"

export default function Page404() {
    const handleGoBack = () => {
        window.location.href = "/";
    }

    return (
        <ContainForms title="404">
            <h1>Page not found</h1>
            <Buttons color="error" label="Go To Home" type="button" method={handleGoBack} />
        </ContainForms>
    )
}