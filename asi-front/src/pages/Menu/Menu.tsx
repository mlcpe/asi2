import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import PaymentsIcon from '@mui/icons-material/Payments';
import SportsEsportsIcon from '@mui/icons-material/SportsEsports';

import NavBar from "../../components/NavBar";
import MenuLink from "../../components/MenuLink";

export default function Menu() {
    return (
        <>
            <NavBar />
            <div className="positionBoxes">
                <MenuLink link="/purchase" color="primary" label="Buy" icon={ShoppingCartIcon} />
                <MenuLink link="/sell" color="secondary" label="Sell" icon={PaymentsIcon} />
                <MenuLink link="/play" color="info" label="Play" icon={SportsEsportsIcon} />
            </div>
        </>
    );
}
