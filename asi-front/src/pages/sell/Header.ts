export const headerTable = [
    {
        id: "Name",
        col: "name",
    },
    {
        id: "Description",
        col: "description",
    },
    {
        id: "Family",
        col: "family",
    },
    {
        id: "Affinity",
        col: "affinity",
    },
    {
        id: "Energy",
        col: "energy",
    },
    {
        id: "HP",
        col: "hp",
    },
    {
        id: "Price",
        col: "price",
    },
];