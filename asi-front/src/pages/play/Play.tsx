import {Alert, Box, Button, Grid} from "@mui/material";
import NavBar from "../../components/NavBar";
import Chat, {socket} from "../../components/Chat";
import Game from "../../components/Game/Game";
import React, {useEffect} from "react";
import {io} from "socket.io-client";
import {InterfaceCard} from "../../components/Interfaces/InterfaceCard";
import CardAPI from "../../services/api/client/CardAPI";
import {ActionJoin, GameMessage} from "../../services/api/types/roomMessage";
import GameCardSelector from "../../components/Game/GameCardSelector";
import {useAppDispatch, useGameState} from "../../services/store";
import {RealRoomState} from "../../services/api/types/room";
import {setGameState, setUserId} from "../../services/store/gameSlice";
import {User} from "../../services/api/types/user";



export function sendGameMessage<K extends GameMessage>(action: K["action"], payload?: K["payload"]) {
    const message: { payload: K["payload"]; action: K["action"] } = {
        action,
        payload
    }
    socket.emit("clientGameEvent", message);
}

const sendStartGameMessage = () => sendGameMessage("start");
const sendEndTurnGameMessage = () => sendGameMessage("endTurn");
const sendRefreshGameMessage = () => sendGameMessage("refresh");

export {
    sendStartGameMessage,
    sendEndTurnGameMessage,
    sendRefreshGameMessage
}

export default function Play() {

    const [currentUser, setCurrentUser] = React.useState<User>({} as any);
    const [_connected, setIsConnected] = React.useState(false);
    const [cards, setCards] = React.useState<InterfaceCard[]>([]);
    const [isLoading, setIsLoading] = React.useState(false);

    const roomState = useGameState();
    const dispatch = useAppDispatch();
    const createGameRoom = async () => {
        const password = prompt("Entrez le mot de passe de la salle :");


        if (!password) return;

        const message: ActionJoin = {
            action: "join",
            payload: {
                password
            }
        }

        socket.emit("clientGameEvent", message);

        /*
        if (response.status === 200) {
            if (response.body.data.roomState === "created")
                setIsLoading(true)
            else
                setIsLoading(false)
            setInRoom(true);
            console.log("Room ID :", response.body);
        }
        */

    };


    useEffect(() => {
        socket.connect();
        socket.emit("userConnect", localStorage.getItem("token") || "");

        function onConnect() {
            console.info("Connected to socket.io server");
            socket.emit("userConnect", localStorage.getItem("token") || "");
            setIsConnected(true);
        }

        function onDisconnect() {
            console.info("Disconnected from socket.io server");
            setIsConnected(false);
        }


        async function onServerGameRefresh(value: RealRoomState) {
            setIsLoading(true);
            console.info("serverGameRefresh", value);
            dispatch(setGameState(value));
            const currentUser = Number(localStorage.getItem("token"));
            // TODO usable because toke is current user id

            dispatch(setUserId(value.user1.id === currentUser ? "user1" : "user2"));
            setIsLoading(false);
        }

        socket.on("connect", onConnect);
        socket.on("disconnect", onDisconnect);
        socket.on("serverGameRefresh", onServerGameRefresh);


        return () => {
            socket.off("connect", onConnect);
            socket.off("disconnect", onDisconnect);
            socket.off("serverGameRefresh", onServerGameRefresh);
        };
    }, []);

    const fetchDatas = async () => {
        const response = await CardAPI.getCards();
        console.log(response);
        if (response?.body?.data) {
            let listingCards: InterfaceCard[] = [];
            response?.body?.data.forEach((card: InterfaceCard) => {
                if (card.userId === Number(localStorage.getItem("token"))) listingCards.push(card);
            })
            setCards(listingCards)
        }
    }

    React.useEffect(() => {
        fetchDatas();
    }, []);
    console.info("roomState", roomState.state !== "NO_ROOM", roomState.state)

    return (
        <>
            <NavBar/>
            <Box sx={{flexGrow: 1, paddingX: 4, marginTop: 2}}>
                <h1>Play</h1>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={3}
                          sx={{display: "flex", flexDirection: "column", justifyContent: "flex-start", gap: 2}}>
                        <Chat currentUser={currentUser} setCurrentUser={setCurrentUser}/>
                    </Grid>
                    <Grid item xs={12} sm={9}>
                        {roomState.state !== "NO_ROOM" ? (
                            roomState.state === "WAITING" ? (
                                <>
                                    <GameCardSelector user={currentUser} roomState={roomState} cards={cards}/>
                                    <Button variant="contained" color="info" size="small"
                                            sx={{width: "100%", borderRadius: 0}}
                                            disabled={!roomState.user1 || !roomState.user2 || Object.keys(roomState.user1.cards).length === 0 || Object.keys(roomState.user2.cards).length === 0}
                                            onClick={sendStartGameMessage}>
                                        Lancer la partie
                                    </Button>
                                </>
                            ) : roomState.state === "STARTED" ? (
                                <Game user={currentUser} roomState={roomState}/>
                            ) : roomState.state === "FINISHED" ? (
                                <div> {roomState[roomState.game.winner].login} à remporter la partie ! </div>
                            ) : (
                                <Alert>En attente de l'autre joueur pour que la partie commence</Alert>
                            )
                        ) : (
                            <Button variant="contained" color="secondary" size="small"
                                    sx={{width: "100%", borderRadius: 0}} onClick={() => createGameRoom()}>
                                Play
                            </Button>
                        )}
                    </Grid>
                </Grid>
            </Box>
        </>
    );
}

