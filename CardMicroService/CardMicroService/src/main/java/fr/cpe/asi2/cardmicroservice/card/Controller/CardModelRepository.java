package fr.cpe.asi2.cardmicroservice.card.Controller;



import fr.cpe.asi2.cardmicroservice.card.model.CardModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CardModelRepository extends CrudRepository<CardModel, Integer> {
    List<CardModel> findByUser(Integer u);
}
