package fr.cpe.asi2.cardmicroservice.card.Controller;

import fr.cpe.asi2.cardmicroservice.card.model.CardReference;
import org.springframework.data.repository.CrudRepository;

public interface CardRefRepository extends CrudRepository<CardReference, Integer> {

}
