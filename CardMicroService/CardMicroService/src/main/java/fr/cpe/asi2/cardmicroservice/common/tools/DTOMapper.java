package fr.cpe.asi2.cardmicroservice.common.tools;

import fr.cpe.asi2.cardmicroservice.card.model.CardDTO;
import fr.cpe.asi2.cardmicroservice.card.model.CardModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DTOMapper {

    public static CardDTO fromCardModelToCardDTO(CardModel cM) {
        CardDTO cDto = new CardDTO(cM);
        return cDto;
    }

    public static List<CardDTO> fromCardModelToCardDTO(Collection<CardModel> ccM) {
        ArrayList<CardDTO> dtoList = new ArrayList<>();

        for (CardModel cardModel : ccM) {
            dtoList.add(fromCardModelToCardDTO(cardModel));
        }

        return dtoList;
    }

    public static CardModel fromCardDtoToCardModel(CardDTO cD) {
        CardModel cm = new CardModel(cD);
        cm.setEnergy(cD.getEnergy());
        cm.setHp(cD.getHp());
        cm.setDefence(cD.getDefence());
        cm.setAttack(cD.getAttack());
        cm.setPrice(cD.getPrice());
        cm.setId(cD.getId());
        return cm;
    }
}
