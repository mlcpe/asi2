package fr.cpe.asi2.usermicroservice.common.tools;

import fr.cpe.asi2.usermicroservice.user.model.UserDTO;
import fr.cpe.asi2.usermicroservice.user.model.UserModel;

public class DTOMapper {


    public static UserDTO fromUserModelToUserDTO(UserModel uM) {
        UserDTO uDto = new UserDTO(uM);
        return uDto;
    }

}
