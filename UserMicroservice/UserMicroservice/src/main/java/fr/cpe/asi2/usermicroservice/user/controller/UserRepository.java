package fr.cpe.asi2.usermicroservice.user.controller;

import fr.cpe.asi2.usermicroservice.user.model.UserModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<UserModel, Integer> {
	
	List<UserModel> findByLoginAndPwd(String login,String pwd);

}
