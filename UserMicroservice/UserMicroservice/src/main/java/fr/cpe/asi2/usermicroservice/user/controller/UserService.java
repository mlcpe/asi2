package fr.cpe.asi2.usermicroservice.user.controller;


import fr.cpe.asi2.cardmicroservice.card.model.CardDTO;
import fr.cpe.asi2.usermicroservice.CardRestService;
import fr.cpe.asi2.usermicroservice.common.tools.DTOMapper;
import fr.cpe.asi2.usermicroservice.user.model.UserDTO;
import fr.cpe.asi2.usermicroservice.user.model.UserModel;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

// import fr.cpe.asi2.usermicroservice.card.Controller.CardModelService;
// import fr.cpe.asi2.usermicroservice.card.model.CardModel

@Service
public class UserService {

    private final UserRepository userRepository;
    private final CardRestService cardRestService;
    // private final CardModelService cardModelService;

    public UserService(UserRepository userRepository, CardRestService cardRestService) { //  CardModelService cardModelService
        this.userRepository = userRepository;
        this.cardRestService = cardRestService;
        // this.cardModelService = cardModelService;
    }

    public List<UserModel> getAllUsers() {
        List<UserModel> userList = new ArrayList<>();
        userRepository.findAll().forEach(userList::add);
        return userList;
    }

    public Optional<UserModel> getUser(String id) {
        return userRepository.findById(Integer.valueOf(id));
    }

    public Optional<UserModel> getUser(Integer id) {
        return userRepository.findById(id);
    }

    public UserDTO addUser(UserDTO user) {
        UserModel u = fromUDtoToUModel(user);
        // needed to avoid detached entity passed to persist error
        UserModel uBd = userRepository.save(u);

        System.out.println("user added: " + uBd.getId());
        userRepository.save(u);

        List<CardDTO> userCards = cardRestService.add(uBd.getId());

        System.out.println(userCards.get(0) + " " + userCards.getClass());

        return DTOMapper.fromUserModelToUserDTO(uBd);
    }

    public UserDTO updateUser(UserDTO user) {
        UserModel u = fromUDtoToUModel(user);
        UserModel uBd = userRepository.save(u);
        return DTOMapper.fromUserModelToUserDTO(uBd);
    }

    public UserDTO updateUser(UserModel user) {
        UserModel uBd = userRepository.save(user);
        return DTOMapper.fromUserModelToUserDTO(uBd);
    }

    public void deleteUser(String id) {
        userRepository.deleteById(Integer.valueOf(id));
    }

    public List<UserModel> getUserByLoginPwd(String login, String pwd) {
        List<UserModel> ulist = null;
        ulist = userRepository.findByLoginAndPwd(login, pwd);
        return ulist;
    }

    private UserModel fromUDtoToUModel(UserDTO user) {
        return new UserModel(user);
    }

}
