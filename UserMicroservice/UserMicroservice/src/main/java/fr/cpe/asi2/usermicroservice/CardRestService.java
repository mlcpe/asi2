package fr.cpe.asi2.usermicroservice;

import fr.cpe.asi2.cardmicroservice.card.model.CardDTO;
import fr.cpe.asi2.common.service.RestService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class CardRestService extends RestService<CardDTO> {

    public CardRestService(@Value("${card.host}") String host, @Value("${card.endpoint}") String endpoint) {
        super(host, endpoint,CardDTO.class);
    }

    @Override
    public List<CardDTO> list() {
        CardDTO[] response = rt.getForObject(
                MessageFormat.format("{0}/", baseUrl),
                CardDTO[].class
        );

        if (response == null) {
            return new ArrayList<>();
        }

        return Arrays.asList(response);
    }

    public final List<CardDTO> add(long id) {
        CardDTO[] response = rt.postForObject(
                MessageFormat.format("{0}/{1}/{2}/", baseUrl, "rand", 5),
                id,
                CardDTO[].class
        );

        if (response == null) {
            return new ArrayList<>();
        }

        return Arrays.asList(response);
    }

}
