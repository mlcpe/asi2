## Diagramme de classe

![alt text](https://zupimages.net/up/23/42/d854.png)

## liste des composants front

- Rouge : Div avec titre qui va contenir les formulaires
- Bleu : Input + Label 
- Orange : Boutons avec/sans icones
- Violet : Bouton pour le menu
- Jaune : Barre de navigation
- Vert : Tableaux (on fait un composant pour une TR + on fait un composant global du tableau avec colonnes dynamiques)
- Marron : Carte

