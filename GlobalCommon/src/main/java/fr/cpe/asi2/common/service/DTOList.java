package fr.cpe.asi2.common.service;

import java.util.ArrayList;
import java.util.List;

public class DTOList<DTO> {
    private List<DTO> dtoList;

    public DTOList() {
        dtoList = new ArrayList<>();
    }

    public void setEmployees(List<DTO> employees) {
        this.dtoList = employees;
    }

    public List<DTO> getCardDTOs() {
        return dtoList;
    }
}