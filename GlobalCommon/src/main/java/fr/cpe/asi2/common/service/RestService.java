package fr.cpe.asi2.common.service;

import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;


public abstract class RestService<DTO> {

    public String baseUrl;
    public final String baseMultipleUrl;
    protected final RestTemplate rt = new RestTemplate();
    protected Class<DTO> defaultClass;
    protected ModelMapper modelMapper;

    public RestService(String host, String endpoint, Class<DTO> defaultClass) {
        super();
        // System.out.println(host+endpoint);
        baseUrl = MessageFormat.format("http://{0}/{1}/", host, endpoint);
        baseMultipleUrl = MessageFormat.format("http://{0}/{1}s/", host, endpoint);
        this.defaultClass = defaultClass;
        modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STANDARD)
                .setFieldMatchingEnabled(true)
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE);
    }

    public final <R> Optional<R> retrieve(long id, Class<R> c) {
        try {
            R ret = rt.getForObject(MessageFormat.format("{0}/{1}/", baseUrl, id), c);
            return Optional.ofNullable(ret);
        } catch (HttpClientErrorException e) {
            return Optional.empty();
        }
    }

    public final Optional<DTO> retrieve(long id) {
        return this.retrieve(id, defaultClass);
    }

    abstract public List<DTO> list();

    public final DTO add(DTO dto) {
        return rt.postForObject(
                MessageFormat.format("{0}/", baseUrl),
                dto,
                defaultClass
        );
    }

    public final DTO update(DTO dto) {
        return rt.postForObject(
                MessageFormat.format("{0}/", baseUrl),
                dto,
                defaultClass
        );
    }

    /*
    public final List<DTO> list(String serializedResponse) {
        return modelMapper.map(serializedResponse, new TypeToken<List<DTO>>() {
        }.getType());
    }

    public final <R> List<R> list(String serializedResponse, Class<R> c) {
        return modelMapper.map(serializedResponse, new TypeToken<List<R>>() {}.getType());
    }
    */

}
