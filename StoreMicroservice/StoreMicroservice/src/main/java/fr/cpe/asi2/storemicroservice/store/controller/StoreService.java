package fr.cpe.asi2.storemicroservice.store.controller;

import fr.cpe.asi2.cardmicroservice.card.model.CardDTO;
import fr.cpe.asi2.storemicroservice.CardRestService;
import fr.cpe.asi2.storemicroservice.UserRestService;
import fr.cpe.asi2.storemicroservice.store.model.StoreAction;
import fr.cpe.asi2.storemicroservice.store.model.StoreTransaction;
import fr.cpe.asi2.usermicroservice.user.model.UserDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class StoreService {

    //private final CardModelService cardService;
    //private final UserService userService;
    private final StoreRepository storeRepository;
    private final CardRestService cardRestService;
    private final UserRestService userRestService;

    public StoreService(StoreRepository storeRepository, CardRestService cardRestService, UserRestService userRestService) { // CardModelService cardService, UserService userService
        this.cardRestService = cardRestService;
        this.userRestService = userRestService;
        this.storeRepository = storeRepository;
    }

    public boolean buyCard(Integer userId, Integer cardId) {

        Optional<CardDTO> cOption = cardRestService.retrieve(cardId);
        Optional<UserDTO> uOption = userRestService.retrieve(userId);

        if (uOption.isEmpty() || cOption.isEmpty()) {
            return false;
        }

        CardDTO c = cOption.get();
        UserDTO u = uOption.get();

        if (c.getUserId() != null)
            return false;

        if (u.getAccount() > c.getPrice()) {
            StoreTransaction sT = new StoreTransaction(userId, cardId, StoreAction.BUY);
            sT = storeRepository.save(sT);

            // int transactionId = sT.getId();

            c.setUserId(u.getId());
            u.setAccount(u.getAccount() - c.getPrice());

            userRestService.update(u);
            cardRestService.update(c);

            return true;
        }
        return false;
    }

    public boolean sellCard(Integer userId, Integer cardId) {

        Optional<CardDTO> cOption = cardRestService.retrieve(cardId);
        Optional<UserDTO> uOption = userRestService.retrieve(userId);

        if (uOption.isEmpty() || cOption.isEmpty())
            return false;


        CardDTO c = cOption.get();
        UserDTO u = uOption.get();

        if (!Objects.equals(c.getUserId(), u.getId()))
            return false;


        StoreTransaction sT = new StoreTransaction(userId, cardId, StoreAction.SELL);
        storeRepository.save(sT);

        c.setUserId(null);
        cardRestService.update(c);
        u.setAccount(u.getAccount() + c.computePrice());

        userRestService.update(u);

        return true;
    }

    public List<StoreTransaction> getAllTransactions() {
        List<StoreTransaction> sTList = new ArrayList<>();
        this.storeRepository.findAll().forEach(sTList::add);
        return sTList;

    }

}
