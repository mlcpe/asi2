package fr.cpe.asi2.storemicroservice;

import fr.cpe.asi2.common.service.RestService;
import fr.cpe.asi2.usermicroservice.user.model.UserDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserRestService extends RestService<UserDTO> {

    public UserRestService(@Value("${user.host}") String host, @Value("${user.endpoint}") String endpoint) {
        super(host, endpoint, UserDTO.class);
    }

    @Override
    public List<UserDTO> list() {
        return null;
    }
}
