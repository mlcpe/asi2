package fr.cpe.asi2.storemicroservice.store.controller;

import  fr.cpe.asi2.storemicroservice.store.model.StoreTransaction;
import org.springframework.data.repository.CrudRepository;

public interface StoreRepository extends CrudRepository<StoreTransaction, Integer> {
	

}
